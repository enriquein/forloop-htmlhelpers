﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Forloop.HtmlHelpers;
using Moq;
using NUnit.Framework;

namespace Forloop.UnitTests
{
    [TestFixture]
    public class ScriptContextTests
    {
        [SetUp]
        public void SetUp()
        {
            _context = new Mock<HttpContextBase>();
            _request = new Mock<HttpRequestBase>();
            _response = new Mock<HttpResponseBase>();
            _items = new Hashtable();
            _writer = new StringWriter(new StringBuilder());

            _context.Setup(x => x.Request).Returns(_request.Object);
            _context.Setup(x => x.Response).Returns(_response.Object);
            _context.Setup(x => x.Items).Returns(_items);
        }

        private Hashtable _items;
        private Mock<HttpContextBase> _context;
        private Mock<HttpRequestBase> _request;
        private Mock<HttpResponseBase> _response;
        private StringWriter _writer;

        private void IsAjaxRequest()
        {
            _request.Setup(x => x["X-Requested-With"]).Returns("XMLHttpRequest");
        }

        [Test]
        public void Ctor_When_Passed_Null_Throws_ArgumentNullException()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => new ScriptContext(null, _writer));
            Assert.That(exception.ParamName, Is.EqualTo("httpContext"));
        }

        [Test]
        public void Dispose_Duplicate_ScriptFiles_Removed_When_ScriptContext_Added_To_HttpContext_Items_And_Not_Ajax_Request()
        {
            var scriptContext = new ScriptContext(_context.Object, _writer);
            var scriptContext2 = new ScriptContext(_context.Object, _writer);
            var scriptfile = "scriptFile";

            scriptContext.AddScriptFile(scriptfile);
            scriptContext2.AddScriptFile(scriptfile);

            scriptContext.Dispose();
            scriptContext2.Dispose();

            Assert.That(scriptContext._scriptFiles, Is.Empty);
        }

        [Test]
        public void Dispose_ScriptContext_Added_To_Existing_Stack_In_HttpContext_Items_When_Not_Ajax_Request()
        {
            var scriptContext = new ScriptContext(_context.Object, _writer);
            var scriptContext2 = new ScriptContext(_context.Object, _writer);

            scriptContext.Dispose();
            scriptContext2.Dispose();

            Assert.That(_items, Is.Not.Empty);
            Assert.That(_items.Count, Is.EqualTo(1));

            var item = _items[ScriptContext.ScriptContextItems] as Stack<ScriptContext>;

            Assert.IsNotNull(item);
            Assert.That(item.First(), Is.SameAs(scriptContext2));
            Assert.That(item.Last(), Is.SameAs(scriptContext));
        }

        [Test]
        public void Dispose_ScriptContext_Added_To_HttpContext_Items_When_Not_Ajax_Request()
        {
            var scriptContext = new ScriptContext(_context.Object, _writer);

            scriptContext.Dispose();

            Assert.That(_items, Is.Not.Empty);
            Assert.That(_items.Count, Is.EqualTo(1));

            var item = _items[ScriptContext.ScriptContextItems] as Stack<ScriptContext>;

            Assert.IsNotNull(item);
            Assert.That(item.First(), Is.SameAs(scriptContext));
        }
    }
}