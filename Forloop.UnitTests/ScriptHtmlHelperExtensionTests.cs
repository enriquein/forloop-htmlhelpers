﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;
using Forloop.HtmlHelpers;
using Moq;
using NUnit.Framework;

namespace Forloop.UnitTests
{
    [TestFixture]
    public class ScriptHtmlHelperExtensionTests
    {
        [SetUp]
        public void Setup()
        {
            _context = new Mock<HttpContextBase>();
            _request = new Mock<HttpRequestBase>();
            _response = new Mock<HttpResponseBase>();
            _output = new StringBuilder();

            _response.Setup(x => x.Write(It.IsAny<string>())).Callback<string>(s => _output.Append(s));

            var items = new Hashtable();

            _context.Setup(x => x.Items).Returns(items);
            _context.Setup(x => x.Request).Returns(_request.Object);
            _context.Setup(x => x.Response).Returns(_response.Object);

            var requestContext = new RequestContext(_context.Object, new RouteData());
            var controllerContext = new ControllerContext(requestContext, new Mock<Controller>().Object);

            _htmlHelper =
                new HtmlHelper(
                    new ViewContext(controllerContext, 
                        new Mock<IView>().Object, 
                        new ViewDataDictionary(), 
                        new TempDataDictionary(), 
                        new StringWriter(_output)), 
                    new Mock<IViewDataContainer>().Object);
        }

        private HtmlHelper _htmlHelper;
        private Mock<HttpRequestBase> _request;
        private Mock<HttpResponseBase> _response;
        private StringBuilder _output;
        private Mock<HttpContextBase> _context;

        private void IsAjaxRequest()
        {
            _request.Setup(x => x["X-Requested-With"]).Returns("XMLHttpRequest");
        }

        private Func<dynamic, HelperResult> ScriptBlock(string script)
        {
            return o => new HelperResult(writer => writer.Write(script));
        }

        private string BuildScriptElementWithSrc(string path)
        {
            return "<script type='text/javascript' src='" + path + "'></script>";
        }

        private string BuildScriptElement(params string[] blocks)
        {
            var builder = new StringBuilder();

            foreach (var block in blocks)
            {
                builder.AppendLine(block);
            }

            return builder.ToString();
        }

        private IHtmlString BuildScriptElementsWithSrc(params string[] paths)
        {
            var builder = new StringBuilder(paths.Length);
            foreach (var path in paths)
            {
                builder.AppendLine(BuildScriptElementWithSrc(path));
            }

            return new HtmlString(builder.ToString());
        }

        [Test]
        public void AddScriptBlock_Adds_ScriptBlock_To_ScriptContext_In_HttpContext_Items()
        {
            var scriptContext = _htmlHelper.BeginScriptContext();

            _htmlHelper.AddScriptBlock(ScriptBlock("script"));

            Assert.That(scriptContext._scriptBlocks, Contains.Item("script"));
        }

        [Test]
        public void AddScriptBlock_When_No_ScriptContext_Throws_InvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _htmlHelper.AddScriptBlock(o => new HelperResult(writer => { })));
        }

        [Test]
        public void AddScriptFile_Adds_Only_One_ScriptFile_When_Duplicate_ScriptFiles()
        {
            var scriptContext = _htmlHelper.BeginScriptContext();
            var path = "path";

            _htmlHelper.AddScriptFile(path);
            _htmlHelper.AddScriptFile(path);

            Assert.That(scriptContext._scriptFiles, Contains.Item(path));
            Assert.That(scriptContext._scriptFiles.Count, Is.EqualTo(1));
        }

        [Test]
        public void AddScriptFile_Adds_ScriptFile_To_ScriptContext_In_HttpContext_Items()
        {
            var scriptContext = _htmlHelper.BeginScriptContext();
            var path = "path";

            _htmlHelper.AddScriptFile(path);

            Assert.That(scriptContext._scriptFiles, Contains.Item(path));
        }

        [Test]
        public void AddScriptFile_When_No_ScriptContext_Throws_InvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _htmlHelper.AddScriptFile("path"));
        }

        [Test]
        public void BeginScriptContext_Adds_ScriptContext_To_HttpContext_Items()
        {
            var scriptContext = _htmlHelper.BeginScriptContext();

            var item = _htmlHelper.ViewContext.HttpContext.Items[ScriptContext.ScriptContextItem] as ScriptContext;

            Assert.IsNotNull(item);
            Assert.That(item, Is.SameAs(scriptContext));
        }

        [Test]
        public void EndScriptContext_Creates_ScriptContexts_In_HttpContext_Items()
        {
            var scriptContext = _htmlHelper.BeginScriptContext();

            _htmlHelper.EndScriptContext();

            var items = _htmlHelper.ViewContext.HttpContext.Items;
            var scriptContexts = items[ScriptContext.ScriptContextItems] as Stack<ScriptContext>;

            Assert.IsNotNull(scriptContexts);
            Assert.That(scriptContexts.First(), Is.SameAs(scriptContext));
        }

        [Test]
        public void RenderScripts_Only_Renders_ScriptFiles_Marked_When_Ajax_Request()
        {
            ScriptContext.Context = _context.Object;
            IsAjaxRequest();

            using (_htmlHelper.BeginScriptContext())
            {
                _htmlHelper.AddScriptFile("path1", true);
                _htmlHelper.AddScriptFile("path2");
                _htmlHelper.AddScriptFile("path3", true);
            }

            Assert.That(_output.ToString(), Is.EqualTo(BuildScriptElementsWithSrc("path1", "path3").ToString()));
        }

        [Test]
        public void RenderScripts_Renders_No_ScriptFiles_When_Ajax_Request()
        {
            ScriptContext.Context = _context.Object;
            IsAjaxRequest();

            using (_htmlHelper.BeginScriptContext())
            {
                _htmlHelper.AddScriptFile("path1");
                _htmlHelper.AddScriptFile("path2");
                _htmlHelper.AddScriptFile("path3");
            }

            Assert.That(_output.ToString(), Is.Empty);
        }

        [Test]
        public void RenderScripts_Writes_ScriptBlocks_After_All_ScriptFiles_In_Top_To_Bottom_Order()
        {
            using (_htmlHelper.BeginScriptContext())
            {
                _htmlHelper.AddScriptFile("path1");
                _htmlHelper.AddScriptBlock(ScriptBlock("block1"));
            }

            using (_htmlHelper.BeginScriptContext())
            {
                _htmlHelper.AddScriptFile("path2");
                _htmlHelper.AddScriptBlock(ScriptBlock("block2"));
            }

            var result = _htmlHelper.RenderScripts(BuildScriptElementsWithSrc);

            Assert.That(result.ToString(), 
                Is.EqualTo(BuildScriptElementsWithSrc("path2", "path1") + BuildScriptElement("block1", "block2")));
        }

        [Test]
        public void RenderScripts_Writes_ScriptBlocks_After_All_ScriptFiles_When_Ajax_Request()
        {
            ScriptContext.Context = _context.Object;
            IsAjaxRequest();

            using (_htmlHelper.BeginScriptContext())
            {
                _htmlHelper.AddScriptFile("path1", true);
                _htmlHelper.AddScriptBlock(ScriptBlock("block1"), true);
                _htmlHelper.AddScriptFile("path2", true);
            }

            Assert.That(_output.ToString(), 
                Is.EqualTo(BuildScriptElementsWithSrc("path1", "path2") + BuildScriptElement("block1")));
        }

        [Test]
        public void RenderScripts_Writes_ScriptFiles_In_Last_In_First_Out_Order()
        {
            using (_htmlHelper.BeginScriptContext())
            {
                _htmlHelper.AddScriptFile("path1");
            }

            using (_htmlHelper.BeginScriptContext())
            {
                _htmlHelper.AddScriptFile("path2");
            }

            var result = _htmlHelper.RenderScripts(BuildScriptElementsWithSrc);

            Assert.That(result.ToString(), Is.EqualTo(BuildScriptElementsWithSrc("path2", "path1").ToString()));
        }
    }
}